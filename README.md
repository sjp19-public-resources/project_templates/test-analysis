These are utility functions for outputting CSV data from Learn to more usable CSV files
for staff. 

# confirm_score.py

To use this code:
1. Add a file called `student_summary_data.csv` to the root directory of this file. 
2. Download the student summary data from Learn, then change the name of the file to `challenge_data.csv`.
3. Add `challenge_data.csv` to the root directory. 
4. Run and execute the file with `python confirm_score.py`

You can add any additional data to the output by adding column numbers to the columns list. 