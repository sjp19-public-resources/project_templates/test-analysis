import csv

columns = [0,1,5,10,11,12]

# for use with the student summary csv file from learn.
# make sure to change the name of the file to student_summary.csv
# if you want additional columns you would add the column number to the
# columns list

# Get the data from the downloaded file
def create_data():
    data_lst = []
    with open('student_summary.csv') as csv_file: 
        reader = csv.reader(csv_file, delimiter=',')

        for row in reader: 
            tmp_data=[]
            for i in columns: 
                tmp_data.append(row[i])
            data_lst.append(tmp_data)
        csv_file.close()      
        return data_lst

# Add the data to a new CSV called student_summary_data.csv
def create_csv(data):
    with open('student_summary_data.csv', 'w') as csv_file: 
        csv_file.truncate()
        writer = csv.writer(csv_file)

        for lst in data: 
            writer.writerow(lst)
        
        csv_file.close()
        
data = create_data()
create_csv(data)
        